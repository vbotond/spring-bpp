package com.epam.sbpp;

import org.springframework.beans.BeansException;
import org.springframework.beans.FatalBeanException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class TextsBeanPostProcessor implements BeanPostProcessor {
    public Object postProcessBeforeInitialization(final Object bean, final String beanName) throws BeansException {
        return bean;
    }

    public Object postProcessAfterInitialization(final Object bean, final String beanName) throws BeansException {
        try {
            if (bean instanceof Texts) {
                final Texts texts = (Texts) bean;
                for (Field f : Texts.class.getDeclaredFields()) {
                    String prefix = "";
                    String postfix = "";
                    for (Annotation a : f.getAnnotations()) {
                        if (a instanceof Prefix) {
                            prefix = ((Prefix) a).value() + " - ";
                        }
                        if (a instanceof Postfix) {
                            postfix = " - " + ((Postfix) a).value();
                        }
                    }
                    f.setAccessible(true);
                    f.set(texts, prefix + f.get(texts) + postfix);
                    f.setAccessible(false);
                }
            }
        } catch (IllegalAccessException exception) {
            throw new FatalBeanException("illegal access", exception);
        }
        return bean;
    }
}
