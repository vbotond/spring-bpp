package com.epam.sbpp;

@DogAgeRatio(ratio = 11)
public class Dog {
    private String name;
    private int age;
    private int dogAge;

    public void setName(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setAge(final int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setDogAge(final int dogAge) {
        this.dogAge = dogAge;
    }

    public int getDogAge() {
        return dogAge;
    }

    @Override public String toString() {
        return "Dog{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", dogAge=" + dogAge +
                '}';
    }
}
