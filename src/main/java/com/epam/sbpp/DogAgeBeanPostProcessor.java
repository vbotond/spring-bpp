package com.epam.sbpp;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import java.lang.annotation.Annotation;

public class DogAgeBeanPostProcessor implements BeanPostProcessor {
    public Object postProcessBeforeInitialization(final Object bean, final String beanName) throws BeansException {
        return bean;
    }

    public Object postProcessAfterInitialization(final Object bean, final String beanName) throws BeansException {
        if (bean instanceof Dog) {
            final Dog dog = (Dog) bean;
            int ratio = 7;
            for (Annotation a : Dog.class.getAnnotations()) {
                if (a instanceof DogAgeRatio) {
                    ratio = ((DogAgeRatio) a).ratio();
                }
            }
            dog.setDogAge(ratio * dog.getAge());
        }
        return bean;
    }
}
