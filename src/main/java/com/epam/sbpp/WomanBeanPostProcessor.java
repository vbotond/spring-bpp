package com.epam.sbpp;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class WomanBeanPostProcessor implements BeanPostProcessor {
    public Object postProcessBeforeInitialization(final Object bean, final String beanName)
            throws BeansException {
        return bean;
    }

    public Object postProcessAfterInitialization(final Object bean, final String beanName)
            throws BeansException {
        Object result = bean;
        if (bean instanceof Person) {
            final Person person = (Person) bean;
            if (person.getGender() == Gender.FEMALE) {
                final Woman woman = new Woman();
                woman.setName(person.getName());
                //                woman.setAge(person.getAge());
                woman.setGender(Gender.FEMALE);
                result = woman;
            }
        }
        return result;
    }
}
