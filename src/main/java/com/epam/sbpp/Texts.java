package com.epam.sbpp;

public class Texts {
    @Prefix("prefix1")
    private String text1;
    @Prefix("prefix2")
    private String text2;
    @Postfix("postfix")
    private String text3;
    @Prefix("prefix")
    @Postfix("postfix")
    private String text4;

    public void setText1(final String text1) {
        this.text1 = text1;
    }

    public String getText1() {
        return text1;
    }

    public void setText2(final String text2) {
        this.text2 = text2;
    }

    public String getText2() {
        return text2;
    }

    public void setText3(final String text3) {
        this.text3 = text3;
    }

    public String getText3() {
        return text3;
    }

    public void setText4(final String text4) {
        this.text4 = text4;
    }

    public String getText4() {
        return text4;
    }

    @Override public String toString() {
        return "Texts{" +
                "text1='" + text1 + '\'' +
                ", text2='" + text2 + '\'' +
                ", text3='" + text3 + '\'' +
                ", text4='" + text4 + '\'' +
                '}';
    }
}
