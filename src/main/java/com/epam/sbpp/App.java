package com.epam.sbpp;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {
        final ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        final Person person = context.getBean("person1", Person.class);
        System.out.println(person);
        final Dog dog = context.getBean("guffy", Dog.class);
        System.out.println(dog);
        final Texts texts = context.getBean("texts", Texts.class);
        System.out.println(texts);
    }
}
