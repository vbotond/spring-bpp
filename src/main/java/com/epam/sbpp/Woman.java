package com.epam.sbpp;

public class Woman extends Person {
    @Override
    public String toString() {
        return "Woman{" +
                "name='" + getName() + '\'' +
                ", age=" + getAge() +
                ", gender=" + getGender() +
                '}';
    }
}
