package com.epam.sbpp;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class GenderBeanPostProcessor implements BeanPostProcessor {
    public Object postProcessBeforeInitialization(final Object bean, final String beanName)
            throws BeansException {
        return bean;
    }

    public Object postProcessAfterInitialization(final Object bean, final String beanName)
            throws BeansException {
        if (bean instanceof Person) {
            final Person person = (Person) bean;
            if (person.getGender() == null) {
                person.setGender(Gender.MALE);
            }
        }
        return bean;
    }
}
